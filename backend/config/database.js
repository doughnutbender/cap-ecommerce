const mongoose = require('mongoose');

const connectDatabase = () => {
    mongoose.connect('mongodb+srv://admin:admin@b106.ysury.mongodb.net/capstone3_ecommerce?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    mongoose.connection.once('open', () => console.log('Connected to database...'))
}

module.exports = connectDatabase;